//customer document
//{
//	_id: "";
//	name: "",
//	contact_no: "",
//	email: ""
//}

var mongoose = require ('mongoose');

module.exports = mongoose.model('Customer', {
    fname: String,
    lname: String,
    contact_no: String,
    email: String
});