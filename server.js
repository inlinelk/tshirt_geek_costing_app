var express = require('express');
var routes = require('./routes.js');
//var mongoose = require('mongoose');
var connect = require('connect');

var app = express();
app.use(connect.json());
app.use(express.static('public'));

//mongoose.connect('mongodb://localhost/test_costing_app');
//var db = mongoose.connection;
//
//db.on('error', function(err) {
//    console.log('Error connecting to DB: \n' + err);
//    console.log('\n**********************\n');
//});

//require('./models/customer.js');

app.get('/', routes.index);

app.get('/server_test', routes.test);

app.post('/customer', routes.setCustomerDetails);

app.post('/order', routes.setOrderDetails);

app.post('/cost', routes.setCostDetails);

//app.get('/customers/add', routes.addCustomer);
//
//app.post('/customers/add', routes.addCustomerResult);

console.log("Listening on port 3000");
app.listen(3000);
