var CostTypeView = Backbone.View.extend({

    initialize : function() {
        
    },
    
    // render : function() {
     
    //     var templateString = "<h3>Order Details</h3>" + 
    //                          "<p>Order Number: <%= order_number %>" +
    //                          "<p>Order Name: <%= order_name %></p>" +
    //                          "<p>Due Date: <%= due_date %> </p>" +
    //                          "<p>Status: <%= status %> </p>" +
    //                          "<p>Quantity: <%= quantity %> </p>" +
    //                          "<p>Image: <%= image_url %> </p>" +
    //                          "<p>Image Decription: <%= image_description %> </p>";
        
    //     this.$el.html(_.template(templateString, this.model.attributes));
    //     return this;
    // }


            render : function() {
        var templateString = "";
        
        $.ajax({
            type: 'GET',
            url: 'js/templates/selectCostType.ejs',
            async: false,
                
        }).done(function(data) {
            templateString = data;
        });
        
        this.$el.html(_.template(templateString, this.model.attributes));
        return this;
    }
});