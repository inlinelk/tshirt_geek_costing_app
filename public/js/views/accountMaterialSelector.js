var AccountMaterialView = Backbone.View.extend({

    initialize : function() {
    	        this.listenTo(this.collection, 'add', this.render);
    },

    render : function() {
        var templateString = "";
        $.ajax({
            type: 'GET',
            url: 'js/templates/accountingSection/accountMaterial.ejs',
            async: false,
        }).done(function(data) {
            templateString = data;
        });
        //console.log(templateString);
        this.$el.html(_.template(templateString, this.collection));
        //console.log(this.collection);
        return this;
    }
});