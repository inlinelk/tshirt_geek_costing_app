var AccountDetailsView = Backbone.View.extend({

    initialize : function() {
    },

    render : function() {
        var templateString = "";
        $.ajax({
            type: 'GET',
            url: 'js/templates/accountDetails.ejs',
            async: false,
        }).done(function(data) {
            templateString = data;
        });
        //console.log(templateString);
        this.$el.html(_.template(templateString));
        return this;
    }
});