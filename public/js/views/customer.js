var CustomerView = Backbone.View.extend({
    
    initialize : function(options) {
        this.listenTo(this.model, 'change', this.render)
    },
    
    // render : function() {
    //     var templateString = "";
        
    //     $.ajax({
    //         type: 'GET',
    //         url: 'js/templates/customer.ejs',
    //         async: false,
                
    //     }).done(function(data) {
    //         console.log(data);  
    //         templateString = data;
    //     });
        
    //     this.$el.html(_.template(templateString, this.model.attributes));
    //     return this;
    // },
        render : function() {
        var templateString = "";
        
        $.ajax({
            type: 'GET',
            url: 'js/templates/addCustomer.ejs',
            async: false,
                
        }).done(function(data) {
            templateString = data;
        });
        
        this.$el.html(_.template(templateString, this.model.attributes));
        return this;
    }
});