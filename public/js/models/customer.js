var CustomerModel = Backbone.Model.extend({
    urlRoot : '/customer',
    defaults : {
        fname: "John",
        lname: "Smith",
        contact_no: "0776543213",
        email: "john@gmail.com"
    }

});