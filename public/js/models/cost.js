var CostModel = Backbone.Model.extend({
    urlRoot: '/cost',
    defaults: {
        type: "TestMaterial",
        description: "testDescription",
        cost: "150",
        order_id: ""
    }
});