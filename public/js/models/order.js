var OrderModel = Backbone.Model.extend({
    urlRoot : '/order',
    defaults : {
        order_number: "TG145678",
        order_name: "Frank Miller",
        due_date: "19/07/2014",
        status: "Pending",
        quantity: "200",
        image_url: "",
        image_description: "",
        customer_id: ""
    }

});