var AppRouter = Backbone.Router.extend({

    routes : {
        "new" : "newEntry",
        "test" : "test",
    },

    initialize : function () {
    //init models
    //this.costModel = new CostModel();
    this.customerModel = new CustomerModel();
    this.orderModel = new OrderModel();

    //init collections
    this.costCollection = new CostCollection();

    //init views
    this.customerView = new CustomerView({
        model : this.customerModel
    });

    this.costView = new CostView({
        collection : this.costCollection   
    });

    this.orderView = new OrderView({
        model : this.orderModel
    });

    this.costSelectorView = new CostSelectorView();

    this.accountDetailsView = new AccountDetailsView();

    this.accountMaterialView = new AccountMaterialView({
        collection : this.costCollection   
    });

    this.accountCollarAndCuffView = new AccountCollarAndCuffView({
        collection : this.costCollection   
    });

    this.accountSewingView = new AccountSewingView({
        collection : this.costCollection   
    });

    this.accountEmbroideryView = new AccountEmbroideryView({
        collection : this.costCollection   
    });

    this.accountPrintingView = new AccountPrintingView({
        collection : this.costCollection   
    });

    this.accountMischelaneousView = new AccountMischelaneousView({
        collection : this.costCollection   
    });

    },

    index : function() {
        alert("building view");
        $('#main').html(this.indexView.render().el);
    },

    newEntry : function() {
        alert("calling newEntry render function");
        $('#main').html(this.customerView.render().el);
        $('#main').append(this.orderView.render().el);
        $('#main').append(this.costSelectorView.render().el);
        $('#cost_breakdown_container > .col-md-12').append(this.costView.render().el)
        $('#main').append(this.accountDetailsView.render().el);
        $('#account_material').append(this.accountMaterialView.render().el);
        $('#account_collar_and_cuff').append(this.accountCollarAndCuffView.render().el);
        $('#account_sewing').append(this.accountSewingView.render().el);
        $('#account_embroidery').append(this.accountEmbroideryView.render().el);
        $('#account_printing').append(this.accountPrintingView.render().el);
        $('#account_mischelaneous').append(this.accountMischelaneousView.render().el);
        $('#accounting_details').hide();
        $('#select_cost_section').hide();
    },

    test : function() {
        alert("test page called");   
    }
});

var app = new AppRouter();

function addToCostCollection(event) {

    if(($("#select_cost_type").val() != '') && ($("#description").val() != '') && ($("#cost").val() != '')){

        app.costCollection.add({type: "" + $('#select_cost_type').val(), description: "" + $('#description').val(), cost: "" + $('#cost').val()});

        $('#description').val('');
        $('#cost').val('');
        
        //app.costCollection.add({type: "Material Cost", description: "Single Jersey", cost:"299.00"});
        console.log("save to db");   
        //app.customerModel.save();
    }
 
}

function saveToDb(event) {

    $('#accounting_details').show();

    app.customerModel.set({
        fname: $('#customer_fname').val(),
        lname: $('#customer_lname').val(),
        contact_no: $('#contact_number').val(),
        email: $('#email').val()
    });

    app.orderModel.set({
        order_number: $('#order_number').val(),
        order_name: $('#order_name').val(),
        due_date: $('#due_date').val(),
        quantity: $('#quantity').val(),
    });
    
    app.customerModel.save(null, {
        error : function(data) {
            alert('Error saving customer information!');
        },
        success : function(data) {
            alert('Information saved successfully');
            //console.log(data.attributes.customer_id);
            //submit the order model for saving
            app.orderModel.set({customer_id: data.attributes.customer_id});
            app.orderModel.save(null, {
                error: function(data) {
                    alert("Error saving order information!");
                },
                success : function(data) {
                    alert("Information saved successfully");
                    //console.log(data.attributes.order_id);
                    //submit the cost collection for saving
                    app.costCollection.each(function(model) {
                        model.set({order_id: data.attributes.order_id});   
                    });
                    var jsonString = JSON.stringify(app.costCollection);
                    //jsonString = jsonString.replace("[", "");
                    //jsonString = jsonString.replace("]", "");
                    console.log(jsonString);
                    
                    $.ajax({
                        url: "/cost",
                        type: "POST",
                        data: jsonString,
                        dataType: "json",
                        contentType: "application/json"
                    }).done(function(data) {
                        console.log(data);   
                    });
                }
            });
        }
    });

}

$(document).ready(function() {
    alert('App is good to go!');
    Backbone.history.start();
    //attach event listener to the save button
    $('#addCostToCollection').click(addToCostCollection);
    $('#saveToDb').click(saveToDb);
});