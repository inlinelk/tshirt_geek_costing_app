var mongojs = require("mongojs");

var uri = "mongodb://localhost/test_costing_app";
var db = mongojs.connect(uri, ["customer", "order", "cost"]);

exports.index = function(req, res) {
	res.sendfile('./public/index.html');
};

exports.test = function(req, res) {
	res.sendfile('./public/server_test.html');
};

exports.setCustomerDetails = function(req, res) {
    var doc = {
        fname: "" + req.body.fname, 
        lname: "" + req.body.lname, 
        contact_no: "" + req.body.contact_no,
        email: "" + req.body.email, 
    };
    
    db.customer.insert(doc, function(err, records) {
        if(err) {
            console.log("Error: " + err);
            res.status(500).json({status: 'faliure'});
        }
        console.log(records);
        res.status(200).json({status: 'success', customer_id: records._id});
    });
};

exports.setOrderDetails = function(req, res) {
 
    var doc = {
        order_number: "" + req.body.order_number,
        order_name: "" + req.body.order_name,
        due_date: "" + req.body.due_date,
        status: "" + req.body.status,
        quantity: "" + req.body.quantity,
        image_url: "" + req.body.image_url,
        image_description: "" + req.body.image_description    
    };

    db.order.insert(doc, function(err, records) {
        if(err) {
            console.log("Error: " + err);
            res.status(500).json({status: 'faliure'});
        }
        console.log(records);
        res.status(200).json({status: 'success', order_id: records._id});
    });
};

exports.setCostDetails = function(req, res) {
    //console.log(req);
        
    for(var i = 0; i < req.body.length; i++) {
        
        var doc = {
        type: req.body[i].type,
        description: req.body[i].description,
        cost: req.body[i].cost,
        order_id: req.body[i].order_id
        };

        db.cost.insert(doc, function(err, records) {
            if(err) {
                console.log("Error: " + err);   
                res.status(500).json({status: 'faliure'});
            }
            console.log(records);
        });
    }
    res.status(200).json({status: 'success'});
};

